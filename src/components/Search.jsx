import React,{useState,useRef} from 'react';
import './search.scss';
import {Link} from 'react-router-dom'

function Search() {
    const refletter = useRef("");


    const [numkey,setNumkey]=useState(false);
    const [isAlpha,setIsAlpha]=useState(true);
    const [input,setInput]=useState("");
    
    

    // api key = 7c709f565f0aa4f98248a3313fd9e5b3
    const handleKeyboard = ()=>{
        setNumkey(!numkey);
        setIsAlpha(!isAlpha);}

    
    const handleletters = (e)=>{
        refletter.current.value+=e.target.innerHTML;

        refletter.current.value=refletter.current.value.toLowerCase();
        
        setInput(refletter.current.value);
        console.log(refletter.current.value+"helloboyds")

    }
    const handlesearch = (e)=>{

        e.preventDefault();
        refletter.current.value=refletter.current.value.toLowerCase();
        setInput(refletter.current.value)
    }
    const handleback = (e)=>{
       refletter.current.value= refletter.current.value.slice(0,refletter.current.value.length-1);
       setInput(refletter.current.value);
    }
    const handleclear = (e)=>{
        setInput("");
    }
    const handlespace = (e)=>{
        refletter.current.value+=" ";
        setInput(refletter.current.value);
    }
    return (
        <div className="searchWrapper">
            <div className="searchIconsHeader">
                
                <div className="icon"><img src="/images/arrow-back.png" alt="back" className="arrow" /> </div>
                <div className="icon"><img src="/images/close-icon.png" alt="close" className="close" /></div>


            </div>
            <div className="searchBar">
                <div className="searchContainer">
                    <div className="icon"><img src="/images/search-icon.png" alt="" className="search" /></div>
                    <div className="search">
                        <form onSubmit={e=>e.preventDefault()}><input type="text" className="searchInput" value={input} placeholder="Search" onChange={handlesearch} ref={refletter}/></form>
                    </div>
                </div>

            </div>
            <div className="searchItemsContainer">
            
                <div className="searchRecentItems">
                <h1>Recent Search Items</h1>
                    <div className="item">
                        <img src="/images/reload-icon.png" alt="" className="movie" />
                        <span>Jathi Ratnalu Movie</span>
                    </div>
                    <div className="item">
                        <img src="/images/reload-icon.png" alt="" className="movie" />
                        <span>Kids English Telugu Dubbed Movies</span>
                    </div>
                    <div className="item">
                        <img src="/images/reload-icon.png" alt="" className="movie" />
                        <span>3D Animation Movies</span>
                    </div>
                    <div className="item">
                        <img src="/images/reload-icon.png" alt="" className="movie" />
                        <span>Action Movies in Telugu 2020</span>
                    </div>
                </div>
                <div className="searchUIKeyboard">
                    <div className={`characters ${isAlpha ? "active" : "notactive"}`}>
                        <div className="row">
                            <div className="letter" onClick={handleletters}>A</div>
                            <div className="letter" onClick={handleletters}>B</div>
                            <div className="letter" onClick={handleletters}>C</div>
                            <div className="letter" onClick={handleletters}>D</div>
                            <div className="letter" onClick={handleletters}>E</div>
                            <div className="letter" onClick={handleletters}>F</div>
                            <div className="letter" onClick={handleletters}>G</div>
                            <div className="letter close" onClick={handleback}><img src="/images/clear-icon.png" alt="" className="del" /></div>
                        </div>
                        <div className="row">
                            <div className="letter" onClick={handleletters}>H</div>
                            <div className="letter" onClick={handleletters}>I</div>
                            <div className="letter" onClick={handleletters}>J</div>
                            <div className="letter" onClick={handleletters}>K</div>
                            <div className="letter" onClick={handleletters}>L</div>
                            <div className="letter" onClick={handleletters}>M</div>
                            <div className="letter" onClick={handleletters}>N</div>
                            <div className="letter num" onClick={handleKeyboard}>123</div>
                        </div>
                        <div className="row">
                            <div className="letter" onClick={handleletters}>O</div>
                            <div className="letter" onClick={handleletters}>P</div>
                            <div className="letter" onClick={handleletters}>Q</div>
                            <div className="letter" onClick={handleletters}>R</div>
                            <div className="letter" onClick={handleletters}>S</div>
                            <div className="letter" onClick={handleletters}>T</div>
                            <div className="letter" onClick={handleletters}>U</div>
                            
                        </div>
                        <div className="row">
                            <div className="letter" onClick={handleletters}>V</div>
                            <div className="letter" onClick={handleletters}>W</div>
                            <div className="letter" onClick={handleletters}>X</div>
                            <div className="letter" onClick={handleletters}>Y</div>
                            <div className="letter" onClick={handleletters}>Z</div>
                            <div className="letter" onClick={handleletters}>-</div>
                            <div className="letter" onClick={handleletters}>'</div>
                        </div>
                        

                    </div>
                    <div className={`numbers ${numkey ? "active" : "notactive"}`}>
                    <div className="row">
                            <div className="letter" onClick={handleletters}>1</div>
                            <div className="letter" onClick={handleletters}>2</div>
                            <div className="letter" onClick={handleletters}>3</div>
                            <div className="letter" onClick={handleletters}>&</div>
                            <div className="letter" onClick={handleletters}>#</div>
                            <div className="letter" onClick={handleletters}>(</div>
                            <div className="letter" onClick={handleletters}>)</div>
                            <div className="letter close" onClick={handleback}><img src="/images/clear-icon.png" alt="" className="del" /></div>
                        </div>
                        <div className="row">
                            <div className="letter" onClick={handleletters}>4</div>
                            <div className="letter" onClick={handleletters}>5</div>
                            <div className="letter" onClick={handleletters}>6</div>
                            <div className="letter" onClick={handleletters}>@</div>
                            <div className="letter" onClick={handleletters}>!</div>
                            <div className="letter" onClick={handleletters}>?</div>
                            <div className="letter" onClick={handleletters}>:</div>
                            <div className="letter num" onClick={handleKeyboard}>& ABC</div>
                        </div>
                        <div className="row">
                            <div className="letter" onClick={handleletters}>7</div>
                            <div className="letter" onClick={handleletters}>8</div>
                            <div className="letter" onClick={handleletters}>9</div>
                            <div className="letter" onClick={handleletters}>0</div>
                            <div className="letter" onClick={handleletters}>.</div>
                            <div className="letter" onClick={handleletters}>_</div>
                            <div className="letter" onClick={handleletters}>"</div>
                            
                        </div>
                    </div>
                    <div className="rowButtons">
                            <div className="btnn" onClick={handlespace}>Space</div>
                            <div className="btnn" onClick={handleclear}>Clear</div>
                            <Link to={`/movies/${input}` } className="btnn search"> Search</Link>
                        </div>
                </div>
            </div>
            
        </div>
    )
}

export default Search
