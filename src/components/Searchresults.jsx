import React,{useState,useEffect} from 'react'
import './searchresults.scss';
import axios from 'axios';
import {Link} from 'react-router-dom';

function Searchresults(props) {
    const [movies,setMovies]=useState([]);
 
    console.log(props);
    useEffect(() => {
        axios.get(`https://api.themoviedb.org/3/search/movie?api_key=7c709f565f0aa4f98248a3313fd9e5b3&language=en-US&page=1&include_adult=false&query=${props.match.params.searchText}`)
            .then(res =>{
                // console.log(page);
                
                setMovies(res.data.results);
             

            })
            .catch(error => console.log(error))

    },[])
    
   

    
    return (
        <div className="searchWrapper">
          
            
            <div className="searchIconsHeader"> 
                <Link to="/" className="icon"><div className="icons"><img src="/images/arrow-back.png" alt="back" className="arrow" /> </div></Link>
                <Link to="/" className="icon"><div className="icons"><img src="/images/close-icon.png" alt="close" className="close" /></div></Link>
            </div>
            <div className="movieresult">
            
            {
               movies.length ? movies.map(movie => (
                    <div className="moviecards">
                        <img src={`https://image.tmdb.org/t/p/w185_and_h278_bestv2${movie.poster_path}`} alt="" className="" />
                        <div className="name">{movie.title}</div>
                    </div>                   
                    
    )) : <div><h1>No data found</h1></div>


            }
            
            </div>
            
           
            
        </div>
    )
}

export default Searchresults
