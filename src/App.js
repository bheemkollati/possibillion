
import './App.css';
import Search from './components/Search';
import Searchresults from './components/Searchresults'
import {BrowserRouter as Router,Switch,Route} from 'react-router-dom'

function App() {
  return (
    <div className="App">
      <Router>
        <Switch>
        <Route path="/movies/:searchText" component={Searchresults}></Route>
          <Route path="/"><Search /></Route>
         
        </Switch>
      </Router>
      
      
    </div>
  );
}

export default App;
